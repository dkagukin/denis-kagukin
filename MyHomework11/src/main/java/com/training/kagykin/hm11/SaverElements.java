package com.training.kagykin.hm11;


import com.training.kagykin.hm5.Element;
import com.training.kagykin.hm5.Folder;

import java.io.*;

public final class SaverElements {
    public static void Save(Folder element, String path) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(path);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(element);
        objectOutputStream.close();
    }

    public static Element Load(String path) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(path);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Element result = (Element) objectInputStream.readObject();
        objectInputStream.close();
        return result;
    }

}
