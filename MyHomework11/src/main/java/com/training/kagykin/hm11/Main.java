package com.training.kagykin.hm11;



import com.training.kagykin.hm5.Builder;
import com.training.kagykin.hm5.Folder;
import com.training.kagykin.hm5.IO;

import java.io.IOException;

public class Main {
    private static final String ROOT_NAME = "C";
    private static final String CHOOSE = "choose action:";
    private static final String ADD = "1 - add";
    private static final String PRINT = "2 - print";
    private static final String EXIT = "3 - exit";
    private static final String SAVE = "4 - save";
    private static final String INPUT_PATH = "input path: ";
    public static final String SAVE_DATA = "upload saved data? (yes/no)";
    public static final String SAVE_PATH = "folder.out";

    public static void main(String[] args) throws IOException {
        IO inOut = new IO();
        boolean loop = true;
        Folder root = new Folder(ROOT_NAME);
        System.out.println(SAVE_DATA);
        if (inOut.input().toLowerCase().equals("yes")) {
            root = tryLoad(root, SAVE_PATH);
        }
        do {
            inOut.output(CHOOSE);
            inOut.output(ADD);
            inOut.output(PRINT);
            inOut.output(EXIT);
            inOut.output(SAVE);
            String answer = inOut.input();
            if (answer.equals("1") || answer.equals("add")) {
                add(root, inOut);
            }
            if (answer.equals("2") || answer.equals("print")) {
                inOut.outputPath(root);
            }
            if (answer.equals("3") || answer.equals("exit")) {
                loop = false;
            }
            if (answer.equals("4") || answer.equals("save")) {
                SaverElements.Save(root, SAVE_PATH);
            }
        } while (loop);
        inOut.close();
    }

    private static void add(Folder root, IO inOut) {
        inOut.output(INPUT_PATH);
        String path = inOut.input();
        try {
            Builder.build(root, path);
        } catch (IllegalArgumentException e) {
            inOut.output(e.getMessage());
        }
    }

    private static Folder tryLoad(Folder root, String path) {
        try {
            root = (Folder) SaverElements.Load(path);
        } catch (ClassNotFoundException | IOException e) {
            System.out.println("no data was found");
        }
        return root;
    }
}
