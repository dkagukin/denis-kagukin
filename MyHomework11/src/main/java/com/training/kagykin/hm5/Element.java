package com.training.kagykin.hm5;

public interface Element {
    String getName();

    Element getElement(int index);

    int size();

    void add(Element element);
}
