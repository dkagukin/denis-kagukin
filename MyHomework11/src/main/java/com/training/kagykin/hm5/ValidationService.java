package com.training.kagykin.hm5;

public final class ValidationService {

    public static final String VALIDATION_REGEX = "((\\w+/?)+)(\\w+\\.?\\w+)?";

    private ValidationService() {
    }

    public static boolean isFolder(String name) {
        return !name.contains(".");
    }

    public static void checkForValidatePath(String path) {
        String regex = VALIDATION_REGEX;
        if (!path.matches(regex)) {
            throw new IllegalArgumentException("Invalid path");
        }
    }
}
