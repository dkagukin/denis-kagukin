package com.training.kagykin.hm5;

public final class Searcher {

    private Searcher() {
    }

    public static boolean isContains(Element root, String name) {
        return (searchElement(root, name) != root);
    }


    public static Element searchElement(Element root, String name) {
        for (int i = 0; i < root.size(); i++) {
            if (root.getElement(i).getName().equals(name)) {
                return root.getElement(i);
            }
        }
        return root;
    }
}
