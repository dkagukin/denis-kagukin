package com.training.kagykin.hm5;

public final class Builder {

    private Builder() {
    }

    public static void build(Folder root, String path) {
        ValidationService.checkForValidatePath(path);
        String[] pathArray = path.split("/");
        Element tempRoot = root;
        for (int i = 0; i < pathArray.length; i++) {
            if (Searcher.isContains(tempRoot, pathArray[i])) {
                tempRoot = Searcher.searchElement(tempRoot, pathArray[i]);
            } else {
                create(tempRoot, pathArray, i);
                return;
            }
        }
    }

    private static void create(Element root, String[] path, int index) {
        Element tempRoot = root;
        Element currentRoot;
        for (int i = index; i < path.length; i++) {
            currentRoot = createElement(path[i]);
            tempRoot.add(currentRoot);
            tempRoot = currentRoot;
        }
    }

    private static Element createElement(String name) {
        if (ValidationService.isFolder(name)) {
            return new Folder(name);
        } else {
            return new File(name);
        }
    }
}
