package com.training.kagykin;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SorterTest {
    private int[] mass = new int[]{0, 1, 2, 15,16,20 };

    @Test
    public void testExecuteSorterSelectionSort() {
        SortingContext strategy = new SelectionSort();
        Sorter context = new Sorter(strategy);
        int[] result = context.executeSorter(new int[]{1,0, 15, 2, 16,20});
        Assert.assertArrayEquals(mass, result);
    }
    @Test
    public void testExecuteSorterSelectionSortNegative() {
        SortingContext strategy = new SelectionSort();
        Sorter context = new Sorter(strategy);
        int[] result = context.executeSorter(null);
        Arrays.equals(null, result);
    }

    @Test
    public void testExecuteSorterBaubleSort() {
        SortingContext strategy = new BubbleSort();
        Sorter context = new Sorter(strategy);
        int[] result = context.executeSorter(new int[]{1,0, 15, 2, 16,20});
        Assert.assertTrue(Arrays.equals(mass, result));
    }
    @Test
    public void testExecuteSorterBaubleSortNegative() {
        SortingContext strategy = new BubbleSort();
        Sorter context = new Sorter(strategy);
        int[] result = context.executeSorter(null);
        Arrays.equals(null, result);
    }
}