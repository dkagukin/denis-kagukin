package com.training.kagykin;

public interface SortingContext {
    int[] sorter(int[] array);
}
