package com.training.kagykin;

public class Sorter {

    private SortingContext sortingContext;

    public Sorter() {

    }

    public Sorter(SortingContext sortingContext) {
        this.sortingContext = sortingContext;
    }

    public int[] executeSorter(int[] array) {
        return sortingContext.sorter(array);
    }
}
