package com.training.kagykin;

public class BubbleSort implements SortingContext {
    int number;

    public int[] sorter(int [] array) {
        if (array != null) {
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array.length - 1; j++) {
                    if (array[j] > array[j + 1]) {
                        number = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = number;
                    }
                }
            }
        } else {
            System.out.println(" Array is null ");
            return array;
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        return array;
    }
}
