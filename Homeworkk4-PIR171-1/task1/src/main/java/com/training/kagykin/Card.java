package com.training.kagykin;

import java.math.BigDecimal;

public abstract class Card {
    private String cardHolderName;
    protected BigDecimal balance;

    public Card(String name, BigDecimal balance) {
        if (Validator.checkPositiveAmount(balance) & Validator.isFullname(name)) {
            cardHolderName = name;
            this.balance = balance;
        } else {
            printMessage(" Name does not match pattern 'Name Surname ");
        }
    }

    public BigDecimal addTo(BigDecimal amount) {
        if (Validator.checkPositiveAmount(amount)) {
            printMessage(" Amount add to your balance " + amount.setScale(2));
        }
        return amount;
    }

    public abstract BigDecimal withDraw(BigDecimal amount);

    public static void printMessage(String str) {
        System.out.println(str);
    }
}
