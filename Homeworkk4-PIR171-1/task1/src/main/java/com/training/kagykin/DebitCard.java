package com.training.kagykin;

import java.math.BigDecimal;

public class DebitCard extends Card {
    private Validator validator;

    public DebitCard(String name, BigDecimal balance) {
        super(name,balance);
    }

    public BigDecimal withDraw(BigDecimal amount) {
        if (balance.compareTo(amount) > 0 & validator.checkPositiveAmount(amount) == false) {
            printMessage(" Amount more than balance. Operation is failed ");
            return amount;
        } else {
            balance = balance.subtract(amount);
            printMessage(" Amount remove " + amount);
        }
        return amount;
    }
}
