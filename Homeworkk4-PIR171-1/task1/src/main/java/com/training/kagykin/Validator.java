package com.training.kagykin;

import java.math.BigDecimal;

public class Validator {
    public static boolean checkPositiveAmount(BigDecimal amount) {
        boolean result = false;
        if (amount.signum() <= 0) {
            System.out.println(" Amount must be more than zero");
            return false;
        } else {
            result = true;
        }
        return result;
    }

    public static boolean checkBalanceInAtm(BigDecimal balanceInAtm, BigDecimal amount) {
        boolean result = false;
        if (amount.compareTo(balanceInAtm) > 0) {
            Card.printMessage(" Not enough cash at Atm. Avaible amount " + balanceInAtm);
            return false;
        } else {
            Card.printMessage(" Operation is successful ");
            result = true;
        }
        return result;
    }

    public static boolean isFullname(String str) {
        String expression = "^([A-Z][a-z]*((\\s)))+[A-Z][a-z]*$";
        return str.matches(expression);
    }
}
