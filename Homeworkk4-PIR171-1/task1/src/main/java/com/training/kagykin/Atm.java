package com.training.kagykin;

import java.math.BigDecimal;

public class Atm {
    private Card card = null;

    private BigDecimal balanceInAtm = new BigDecimal(5000);

    public Atm(BigDecimal balanceInAtm) {
        if (Validator.checkPositiveAmount(balanceInAtm)) {
            this.balanceInAtm = balanceInAtm;
        }
    }

    public Atm(Card operatingCard) {
        card = operatingCard;
    }

    public BigDecimal withDraw(BigDecimal amount) throws Exception {
        if (card.withDraw(amount).signum() > 0  && withDrawAtmBalance(amount).signum() > 0) {
            return amount;
        }
        throw new Exception(" Operation error ");
    }

    public BigDecimal addTo(BigDecimal amount) {
        card.addTo(amount);
        balanceInAtm = balanceInAtm.add(amount);
        return amount;
    }

    public BigDecimal withDrawAtmBalance(BigDecimal amount) {
        if (Validator.checkBalanceInAtm(balanceInAtm,amount)) {
            balanceInAtm = balanceInAtm.subtract(amount);
            card.printMessage(" Take your cash ");
        }
        return amount;
    }
}
