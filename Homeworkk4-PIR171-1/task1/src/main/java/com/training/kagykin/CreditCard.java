package com.training.kagykin;

import java.math.BigDecimal;

public class CreditCard extends Card {
    private Validator validator;

    public CreditCard(String name, BigDecimal amount) {
        super(name, amount);
    }

    public BigDecimal withDraw(BigDecimal amount) {
        if (validator.checkPositiveAmount(amount)) {
            balance = balance.subtract(amount);
            printMessage(" Ammount remove " + amount);
        }
        return amount;
    }
}
