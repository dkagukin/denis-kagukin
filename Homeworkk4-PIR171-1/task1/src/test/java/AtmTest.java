import com.training.kagykin.Atm;
import com.training.kagykin.CreditCard;
import com.training.kagykin.DebitCard;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class AtmTest {
    Atm atm = new Atm(new BigDecimal(5000));
    DebitCard debitCard = new DebitCard("Test Test", new BigDecimal(5000));
    CreditCard creditCard = new CreditCard("Test Test", new BigDecimal(5000));

    @Test
    void withDrawPositiveForDebitCard() {
        BigDecimal result = debitCard.withDraw(new BigDecimal(500));
        Assert.assertEquals(new BigDecimal(500),result);
    }
    @Test
    void withDrawNegativeForDebitCard() {
        BigDecimal result = debitCard.withDraw(new BigDecimal(-500));
        Assert.assertEquals(new BigDecimal(-500),result);
    }

    @Test
    void addToPositiveForDebitCard() {
        BigDecimal result = debitCard.addTo(new BigDecimal(500));
        Assert.assertEquals(new BigDecimal(500), result);
    }
    @Test
    void addToNeagtiveForDebitCard() {
        BigDecimal result = debitCard.addTo(new BigDecimal(-500));
        Assert.assertEquals(new BigDecimal(-500), result);
    }
    @Test
    void withDrawAtmBalancePositive() {
        BigDecimal result = atm.withDrawAtmBalance(new BigDecimal(5000));
        Assert.assertEquals(new BigDecimal(5000),result);
    }
    @Test
    void withDrawAtmBalanceNegative() {
        BigDecimal result = atm.withDrawAtmBalance(new BigDecimal(5500));
        Assert.assertEquals(new BigDecimal(5500),result);
    }
    @Test
    void withDrawPositiveCreditCard() {
        BigDecimal result = creditCard.withDraw(new BigDecimal(500));
        Assert.assertEquals(new BigDecimal(500),result);
    }
    @Test
    void withDrawNegativeCreditCard() {
        BigDecimal result = creditCard.withDraw(new BigDecimal(-500));
        Assert.assertEquals(new BigDecimal(-500),result);
    }

    @Test
    void addToPositiveForCreditCard() {
        BigDecimal result = creditCard.addTo(new BigDecimal(500));
        Assert.assertEquals(new BigDecimal(500), result);
    }
    @Test
    void addToNeagtiveForCreditCard() {
        BigDecimal result = creditCard.addTo(new BigDecimal(-500));
        Assert.assertEquals(new BigDecimal(-500), result);
    }

}