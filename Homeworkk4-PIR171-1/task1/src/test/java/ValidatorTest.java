import com.training.kagykin.Validator;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {

    @Test
    void checkPositiveAmount() {
       boolean result = Validator.checkPositiveAmount(new BigDecimal(500));
        Assert.assertEquals(true,result);
    }
    @Test
    void checkNegativeAmount() {
        boolean result = Validator.checkPositiveAmount(new BigDecimal(-500));
        Assert.assertEquals(false,result);
    }

    @Test
    void checkBalanceInAtmPositive() {
        boolean result = Validator.checkBalanceInAtm(new BigDecimal(5000),new BigDecimal(4500));
        Assert.assertEquals(true,result);
    }
    @Test
    void checkBalanceInAtmNegative() {
        boolean result = Validator.checkBalanceInAtm(new BigDecimal(5000),new BigDecimal(5500));
        Assert.assertEquals(false,result);
    }

    @Test
    void isFullnamePositive() {
        boolean result = Validator.isFullname("Test Test");
        Assert.assertEquals(true,result);
    }
    @Test
    void isFullnameNegative() {
        boolean result = Validator.isFullname("Testest");
        Assert.assertEquals(false,result);
    }
}