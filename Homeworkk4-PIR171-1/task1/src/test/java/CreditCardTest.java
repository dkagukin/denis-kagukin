import com.training.kagykin.CreditCard;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CreditCardTest {
CreditCard creditCard = new CreditCard("Test Test", new BigDecimal(300));
   // Добавить result переменную и сравнивать с ней
    @Test
    void withDrawPositive() {
        BigDecimal result = creditCard.withDraw(new BigDecimal(500));
        Assert.assertEquals(new BigDecimal(500),result);
    }
    @Test
    void withDrawNegative() {
        BigDecimal result = creditCard.withDraw(new BigDecimal(-500));
        Assert.assertEquals(new BigDecimal(-500),result);
    }
}