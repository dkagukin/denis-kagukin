import com.training.kagykin.Card;
import org.junit.Assert;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {
    Card card = new Card("Test Test", new BigDecimal(500)) {
    public BigDecimal withDraw(BigDecimal amount) {
        return null;
    }
};
    @org.junit.jupiter.api.Test
    void addToPositive() {
        BigDecimal result = card.addTo(new BigDecimal(500));
        Assert.assertEquals(new BigDecimal(500),result);
    }
    @org.junit.jupiter.api.Test
    void addToNegative() {
        BigDecimal result = card.addTo(new BigDecimal(0));
        Assert.assertEquals(new BigDecimal(0),result);
    }
}