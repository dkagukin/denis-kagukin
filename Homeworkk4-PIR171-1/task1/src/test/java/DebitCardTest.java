import com.training.kagykin.Card;
import com.training.kagykin.DebitCard;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class DebitCardTest {
    Card debitCard = new DebitCard("Test Test", new BigDecimal(200));
    @Test
    void withDrawNegative() {
        BigDecimal result = debitCard.withDraw(new BigDecimal(300));
        Assert.assertEquals(new BigDecimal(300),result);
    }
    @Test
    void withDrawPositive() {
        BigDecimal result = debitCard.withDraw(new BigDecimal(150));
        Assert.assertEquals(new BigDecimal(150),result);
    }
}