package com.training.kagykin;

import java.util.Arrays;

public class Median {
    public static float median(int[] inputArray) {
        int[] arrayclone = Arrays.copyOf(inputArray,inputArray.length);
        Arrays.sort(arrayclone);
        float result = 0;
        if (arrayclone.length % 2 == 0) {
            result = (float) (arrayclone[arrayclone.length / 2 - 1] + arrayclone[arrayclone.length / 2]) / 2;
        } else {
            result = (float) arrayclone[arrayclone.length / 2];
        }
        return result;
    }

    public static double median(double[] inputArray) {
        double[] arrayclone = Arrays.copyOf(inputArray,inputArray.length);
        Arrays.sort(arrayclone);
        double result = 0;
        if (arrayclone.length % 2 == 0) {
            result = (arrayclone[arrayclone.length / 2 - 1] + arrayclone[arrayclone.length / 2]) / 2;
        } else {
            result = arrayclone[arrayclone.length / 2];
        }
        return result;
    }
}
