package com.training.kagykin;

import java.math.BigDecimal;

public class Card {
    private String cardHolderName;
    private BigDecimal balance;
    public BigDecimal[] differentcurrencies = new BigDecimal[3];
    private String[] currency = new String[]{"BYN","USD","EUR"};

    public Card(String name, BigDecimal balance) {
        cardHolderName = name;
        this.balance = balance;
    }

    public Card(String name) {
        this(name, BigDecimal.ZERO);
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public BigDecimal addTo(BigDecimal amount) {
        try {
            if (amount.signum() <= 0) {
                throw new Exception("Amount must be not negative. Please repeat the action");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return amount;
        }
        balance = balance.add(amount);
        return amount;
    }

    public BigDecimal withdraw(BigDecimal amount) {
        try {
            if (amount.signum() <= 0) {
                throw new Exception(" Amount will be positive");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return amount;
        }
        try {
            if (balance.signum() <= 0) {
                throw new Exception("Not enough money, enter another amount");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return balance;
        }
        balance = balance.subtract(amount);
        return amount;
    }

    public BigDecimal displayBalance() {
        System.out.println(" Your balance is : ");
        for (int i = 0; i < differentcurrencies.length; i++) {
            convertedBalance();
            System.out.println(differentcurrencies[i] + " " + currency[i]);
        }
        return balance;
    }

    public BigDecimal[] convertedBalance() {
        differentcurrencies[0] = getBalance();
        for (int i = 1; i <= 2; i++) {
            differentcurrencies[i] = balance.multiply(new BigDecimal(0.5)
                    .setScale(3, BigDecimal.ROUND_CEILING));
        }
        return differentcurrencies;
    }
}
