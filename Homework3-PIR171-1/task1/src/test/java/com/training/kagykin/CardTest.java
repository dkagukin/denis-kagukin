package com.training.kagykin;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CardTest {
    Card card = new Card("Test", new BigDecimal(0.00));

    @Test
    public void getCardHolderName(){
    Assert.assertEquals(card.getCardHolderName(),"Test");
    }

    @Test
    public void addToNegative() {
        card.addTo(new BigDecimal(-900));
        Assert.assertEquals(-900,-900);
    }
    @Test
    public void addToPositive() {
        card.addTo(new BigDecimal(900));
        Assert.assertEquals(900,900);
    }
    @Test
    public void withdrawNegative() {
        card.withdraw(new BigDecimal(-985));
        Assert.assertEquals(-985,-985);
        card.withdraw(new BigDecimal(500));
        Assert.assertEquals(card.getBalance(),new BigDecimal(0));
    }
    @Test
    public void withDrawPositive() {
        Card card = new Card("Test", new BigDecimal(300));
        card.withdraw(new BigDecimal(200));
        Assert.assertEquals(card.getBalance(),new BigDecimal(100));
    }
    @Test
    public void displayBalance(){
        Card card = new Card("Test",new BigDecimal(600));
        card.displayBalance();
        Assert.assertEquals(card.getBalance(),new BigDecimal(600));
    }
    @Test
    public void convertedBalance(){
        Card card = new Card("Test",new BigDecimal(600));
        card.convertedBalance();
        for(int i = 0; i < card.differentcurrencies.length;i++){
            Assert.assertEquals(card.differentcurrencies[i],new BigDecimal(600));
            i++;
            Assert.assertEquals(card.differentcurrencies[i],new BigDecimal(300).setScale(3));
            i++;
            Assert.assertEquals(card.differentcurrencies[i],new BigDecimal(300).setScale(3));
        }
    }
}