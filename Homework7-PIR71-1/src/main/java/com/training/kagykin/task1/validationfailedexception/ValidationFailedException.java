package com.training.kagykin.task1.validationfailedexception;

public class ValidationFailedException extends Exception {
    public ValidationFailedException(String message) {
        super(message);
    }

}
