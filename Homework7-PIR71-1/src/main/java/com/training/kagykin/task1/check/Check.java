package com.training.kagykin.task1.check;

public class Check<T> {

    public void isEmpty(T input) {
        if (input == null || input == "") {
            System.out.println("Incorrect input. Mustn't be empty ");
        }
    }
}
