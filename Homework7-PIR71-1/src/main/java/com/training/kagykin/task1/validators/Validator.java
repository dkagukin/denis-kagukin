package com.training.kagykin.task1.validators;

import com.training.kagykin.task1.validationfailedexception.ValidationFailedException;

public interface Validator<T> {
    void validate(T s) throws ValidationFailedException;
}

