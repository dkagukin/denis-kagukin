package com.training.kagykin.task1.validators;

import com.training.kagykin.task1.validationfailedexception.ValidationFailedException;

public class IntegerValidator implements Validator<Integer> {

    @Override
    public void validate(Integer number) throws ValidationFailedException {
        if (number < 1 || number > 10) {
            throw new ValidationFailedException("Wrong Integer format");
        }
    }
}
