package com.training.kagykin.task1.validationsystem;

import com.training.kagykin.task1.check.Check;
import com.training.kagykin.task1.validationfailedexception.ValidationFailedException;
import com.training.kagykin.task1.validators.IntegerValidator;
import com.training.kagykin.task1.validators.StringValidator;
import com.training.kagykin.task1.validators.Validator;

public class ValidationSystem {
    public static Validator validator;

    public static void validate(Integer number) throws ValidationFailedException {
        new Check().isEmpty(number);
        validator = new IntegerValidator();
        validator.validate(number);
    }

    public static void validate(String input) throws ValidationFailedException {
        new Check().isEmpty(input);
        validator = new StringValidator();
        validator.validate(input);
    }
}
