package com.training.kagykin.validator;

import com.training.kagykin.validatorexception.ValidatorException;
import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {

    private Validator validator = new Validator();
    private final String[] array = new String[]{"Abc","acs","wer"};
    private final String[] arrayWithRussianWords = new String[]{"Кот","Собака","сад"};
    private final String[] arrayNull = new String[]{"","",""};
    @Test
    public void isStringContainsLatinCharactersOnlyPositive() throws Exception {
        String str = "";
        for(int i =0 ; i< array.length;i++) {
            str += array[i];
        }
    Assert.assertEquals(validator.isStringContainsLatinCharactersOnly(array),str.matches("^[a-zA-Z\\s,]+$"));
    }
    @Test(expected = ValidatorException.class)
    public void isStringContainsLatinCharactersOnlyNegative() throws ValidatorException {
        validator.isStringContainsLatinCharactersOnly(arrayWithRussianWords);
    }
    @Test
    public void isStringNotNullPositive() throws ValidatorException {
        boolean usl = validator.isStringNotNull(array);
        Assert.assertEquals(validator.isStringNotNull(array),usl);
    }
    @Test(expected = ValidatorException.class)
    public void isStringNotNullNegative() throws ValidatorException {
      validator.isStringNotNull(arrayNull);
    }
    @Test
    public void isMultipleCheckPositive() throws ValidatorException {
        Assert.assertEquals(validator.isMultipleCheck(array),true);
    }

    @Test(expected = ValidatorException.class)
    public void isMultipleCheckNegative() throws ValidatorException {
        validator.isMultipleCheck(arrayWithRussianWords);
    }
}