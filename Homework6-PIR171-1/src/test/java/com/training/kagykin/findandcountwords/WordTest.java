package com.training.kagykin.findandcountwords;

import org.junit.Assert;
import org.junit.Test;

public class WordTest {
    private final String[] array = new String[] {"Once", "upon", "a", "time", "a", "Wolf", "was", "lapping", "at", "a", "spring",
        "on", "a", "hillside,", "when,", "looking", "up,", "what", "should", "he", "see", "but", "a",
        "Lamb", "just", "beginning", "to", "drink", "a", "little", "lower", "down"};
    GetWordAndCount getWordAndCount = new GetWordAndCount();
    @Test
    public void comprasionWordsInArray() {
        Assert.assertEquals(getWordAndCount.getListForWords().size(), getWordAndCount.getListForQuantity().size());
    }

    @Test
    public void castToLowerCase() {
        String[] additionalArray = new String[32];
        for(int i =0; i < array.length; i++) {
            additionalArray[i] = array[i].toLowerCase();
        }
        Assert.assertEquals(getWordAndCount.castToLowerCase(array),additionalArray);
    }

    @Test
    public void sortArray() {
    }

    @Test
    public void getQuantityWords() {
        String[] testArray = new String[]{"a","a","a","a"};
        Assert.assertEquals(getWordAndCount.getQuantityWords(testArray, "a"),testArray.length);
    }

    @Test
    public void deleteComma() {
    }
}