package com.training.kagykin.main;

import com.training.kagykin.findandcountwords.GetWordAndCount;
import com.training.kagykin.validator.Validator;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) throws Exception {
        String inputRow ="";
        GetWordAndCount words = new GetWordAndCount();
        while (true) {
            Validator validator = new Validator();
            System.out.println(" Input string or 'Print' - input result or 'Exit'");
            Scanner scan = new Scanner(System.in);
            inputRow = scan.nextLine();
            if (inputRow.equals("exit") || inputRow.equals("Exit")) {
                break;
            }
            if (inputRow.equals("print") || inputRow.equals("Print")) {
                words.printResult();
            } else {
                System.out.println("If the instructions do not appear on the screen, press enter");
                inputRow += scan.nextLine();
                String[] array = inputRow.split(" ");
                try {
                    if (validator.isMultipleCheck(array)) {
                        words.comprasionWordsInArray(array);
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
                array = null;
            }
        }
    }
}
