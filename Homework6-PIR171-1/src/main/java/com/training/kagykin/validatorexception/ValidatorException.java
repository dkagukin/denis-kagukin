package com.training.kagykin.validatorexception;


public class ValidatorException extends Exception {

    public ValidatorException(String message) {
        super(message);
    }
}
