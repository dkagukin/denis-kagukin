package com.training.kagykin.findandcountwords;

import com.training.kagykin.validator.Validator;

import java.util.ArrayList;
import java.util.Arrays;

public class GetWordAndCount {
    private char[] englishAlphabet = {'a','b','c','d','e','f','g','h','i','j',
            'k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    private String[] arrayOfStrings;
    private final ArrayList<String> listForWords = new ArrayList<>();
    private final ArrayList<Integer> listForQuantity = new ArrayList<>();


    public void comprasionWordsInArray(String[] array) throws Exception {
        setArrayOfStrings(array);
        String word;
        castToLowerCase(arrayOfStrings);
        sortArray(arrayOfStrings);
        arrayOfStrings = deleteComma(arrayOfStrings);
        int index = 0;
        String previousWord = "";
        for (int i = 0; i < arrayOfStrings.length; i++) {
            if (arrayOfStrings[i].toCharArray()[0] == englishAlphabet[index]) {
                word = arrayOfStrings[i];
                if (previousWord == "") {
                    previousWord = word;
                    listForQuantity.add(getQuantityWords(arrayOfStrings, word));
                    listForWords.add(word);
                } else {
                    if (!previousWord.equals(word)) {
                        previousWord = word;
                        listForQuantity.add(getQuantityWords(arrayOfStrings, word));
                        listForWords.add(word);
                    }
                }
            } else {
                index++;
                i--;
            }
        }
    }

    public String[] castToLowerCase(String[] isArrayToLowerCase) {
        for (int i = 0; i < isArrayToLowerCase.length; i++) {
            isArrayToLowerCase[i] = isArrayToLowerCase[i].toLowerCase();
        }
        return isArrayToLowerCase;
    }

    public String[] sortArray(String[] isArrayToSort) {
        Arrays.sort(isArrayToSort);
        return isArrayToSort;
    }

    public int getQuantityWords(String[] array, String word) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(word)) {
                count++;
            }
        }
        return count;
    }

    public void printResult() {
        int index = 0;
        for (int i = 0; i < listForWords.size(); i++) {
            if (i == 0) {
                System.out.println(Character.toUpperCase(englishAlphabet[index]));
            }
            if (listForWords.get(i).toCharArray()[0] == englishAlphabet[index]) {
                System.out.println("\t" + listForWords.get(i) + "\t" + listForQuantity.get(i));
            } else {
                i--;
                index++;
                System.out.println(Character.toUpperCase(englishAlphabet[index]));
            }
        }
    }

    public String[] deleteComma(String[] array) {
        String str = "";
        for (int i = 0; i < array.length; i++) {
            str += array[i] + " ";
        }
        str =  str.replace(",","");
        array = str.split(" ");
        return array;
    }

    public ArrayList<String> getListForWords() {
        return listForWords;
    }

    public ArrayList<Integer> getListForQuantity() {
        return listForQuantity;
    }

    public void setArrayOfStrings(String[] arrayOfStrings) throws Exception {
        if (new Validator().isMultipleCheck(arrayOfStrings)) {
            this.arrayOfStrings = arrayOfStrings;
        }
    }
}
