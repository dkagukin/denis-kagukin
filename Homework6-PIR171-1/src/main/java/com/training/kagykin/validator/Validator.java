package com.training.kagykin.validator;

import com.training.kagykin.validatorexception.ValidatorException;

import javax.xml.bind.ValidationException;

public class Validator {

    public boolean isStringContainsLatinCharactersOnly(String[] isArrayToCheck) throws ValidatorException {
        String isStringToCheck = "";
        for (int i = 0; i < isArrayToCheck.length; i++) {
            isStringToCheck += isArrayToCheck[i] + " ";
        }
        if (!isStringToCheck.matches("^[a-zA-Z\\s,.]+$")) {
            throw new ValidatorException(" Only English text can be processed ");
        }
        return isStringToCheck.matches("^[a-zA-Z\\s,.]+$");
    }

    public boolean isStringNotNull(String[] isArrayToCheck) throws ValidatorException {
        int count = 0;
        for (int i = 0; i < isArrayToCheck.length; i++) {
            if (isArrayToCheck[i].equals("") || isArrayToCheck[i].equals(" ")) {
                count++;
            }
        }
        if (count == isArrayToCheck.length) {
            throw new ValidatorException(" The string cannot be empty ");
        }
        return true;
    }

    public boolean isMultipleCheck(String[] isArrayToCheck) throws ValidatorException {
        if (isStringNotNull(isArrayToCheck) && isStringContainsLatinCharactersOnly(isArrayToCheck)) {
            return true;
        }
        return false;
    }
}
