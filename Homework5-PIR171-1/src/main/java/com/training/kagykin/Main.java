package com.training.kagykin;

public class Main {

    public static final String ROOT_NAME = "C";
    public static final String CHOOSE = "choose action:";
    public static final String ADD = "1 - add";
    public static final String PRINT = "2 - print";
    public static final String EXIT = "3 - exit";
    public static final String INPUT_PATH = "input path: ";

    public static void main(String[] args) {
        IO inOut = new IO();
        boolean loop = true;
        Folder root = new Folder(ROOT_NAME);
        do {
            inOut.output(CHOOSE);
            inOut.output(ADD);
            inOut.output(PRINT);
            inOut.output(EXIT);
            String answer = inOut.input();
            if (answer.equals("1") || answer.equals("add")) {
                add(root, inOut);
            }
            if (answer.equals("2") || answer.equals("print")) {
                inOut.outputPath(root);
            }
            if (answer.equals("3") || answer.equals("exit")) {
                loop = false;
            }
        } while (loop);
        inOut.close();
    }

    public static void add(Folder root, IO inOut) {
        inOut.output(INPUT_PATH);
        String path = inOut.input();
        try {
            Builder.build(root, path);
        } catch (IllegalArgumentException e) {
            inOut.output(e.getMessage());
        }
    }
}
