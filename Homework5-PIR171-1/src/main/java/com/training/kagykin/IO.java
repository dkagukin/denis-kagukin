package com.training.kagykin;

import java.util.Scanner;

public class IO {
    public static final String INDENT = "";
    public static final String SLASH = "/";
    public static final String SPACE = "  ";
    private Scanner scanner;

    public IO() {
        scanner = new Scanner(System.in);
    }

    public void output(String string) {
        System.out.println(string);
    }

    public void outputPath(Folder folder) {
        for (Element element : (folder).getElements()) {
            printAll(element, INDENT);
        }
    }

    public String input() {
        return scanner.nextLine();
    }

    public void close() {
        scanner.close();
    }

    private void printAll(Element element, String tab) {
        if (ValidationService.isFolder(element.getName())) {
            System.out.println(tab + element + SLASH);
        } else {
            System.out.println(tab + element);
        }
        tab += SPACE;
        for (int i = 0; i < element.size(); i++) {
            printAll(element.getElement(i), tab);
        }
    }

}

