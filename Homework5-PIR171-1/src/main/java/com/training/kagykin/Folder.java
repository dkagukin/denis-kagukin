package com.training.kagykin;

import java.util.ArrayList;

public class Folder implements Element {
    private String name;

    private ArrayList<Element> elements;

    public Folder(String name) {
        this.name = name;
        elements = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    public ArrayList<Element> getElements() {
        return elements;
    }

    @Override
    public Element getElement(int index) {
        return elements.get(index);
    }

    @Override
    public void add(Element element) {
        elements.add(element);
    }

    @Override
    public int size() {
        return elements.size();
    }

    @Override
    public String toString() {
        return name;
    }
}
