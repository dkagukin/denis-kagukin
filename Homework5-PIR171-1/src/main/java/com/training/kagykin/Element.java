package com.training.kagykin;

public interface Element {
    String getName();

    Element getElement(int index);

    int size();

    void add(Element element);
}
