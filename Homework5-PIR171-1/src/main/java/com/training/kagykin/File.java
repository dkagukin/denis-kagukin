package com.training.kagykin;

public class File implements Element {
    private String name;

    public File(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Element getElement(int index) {
        return this;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public void add(Element element) {
    }

    @Override
    public String toString() {
        return name;
    }
}
