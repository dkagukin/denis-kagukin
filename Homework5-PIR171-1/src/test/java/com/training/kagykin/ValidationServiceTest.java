package com.training.kagykin;

import org.junit.Assert;
import org.junit.Test;

public class ValidationServiceTest {
    private static final String FOLDER_NAME = "Folder";
    private static final String INVALID_FOLDER_NAME = "Folder.new";
    private static final String PATH = "/folder/folder1/file.new";
    private static final String EMPTY_PATH = "";
    private static final String INVALID_PATH = "//file.new/folder//";

    @Test
    public void testForFolder() {
        boolean result = ValidationService.isFolder(FOLDER_NAME);
        Assert.assertTrue(result);
    }

    @Test
    public void testForFile() {
        boolean result = ValidationService.isFolder(INVALID_FOLDER_NAME);
        Assert.assertFalse(result);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testEmptyPath() {
        ValidationService.checkForValidatePath(EMPTY_PATH);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInvalidPath() {
        ValidationService.checkForValidatePath(INVALID_PATH);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInvalidFolderName() {
        ValidationService.checkForValidatePath(PATH);
    }
}