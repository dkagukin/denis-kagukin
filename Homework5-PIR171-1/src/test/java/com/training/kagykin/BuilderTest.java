package com.training.kagykin;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BuilderTest {

    private static final String ROOT_NAME = "root";
    private static final String PATH_1 = "folder";
    private static final String PATH_2 = "file.new";
    private static final String INVALID_PATH = "//file.new/folder//";

    private Folder root;

    @Before
    public void init() {
        root = new Folder(ROOT_NAME);
    }

    @Test
    public void testBuildFolder() {
        Builder.build(root, PATH_1);
        Assert.assertEquals(root.size(), 1);
    }

    @Test
    public void testBuildFile() {
        Builder.build(root, PATH_2);
        Assert.assertEquals(root.size(), 1);
    }

    @Test
    public void testBuildFolderAndFile() {
        Builder.build(root, PATH_1);
        Builder.build(root, PATH_2);
        Assert.assertEquals(root.size(), 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBuildInvalidPath() {
        Builder.build(root, INVALID_PATH);
    }
}