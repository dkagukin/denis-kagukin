package com.training.kagykin;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SearcherTest {
    private static final String ROOT_NAME = "root";
    private static final String FOLDER_NAME = "Folder";
    private static final String ANOTHER_FOLDER = "anotherFolder";
    private static final String FILE_NAME = "file.txt";
    private static final String ANOTHER_FILE = "anotherFile.txt";

    private Folder root;
    private Folder folder;
    private File file;

    @Before
    public void init() {
        root = new Folder(ROOT_NAME);
        folder = new Folder(FOLDER_NAME);
        file = new File(FILE_NAME);
    }
    @Test
    public void testFolderContainsFolder() {
        root.add(folder);
        boolean result = Searcher.isContains(root, FOLDER_NAME);
        Assert.assertTrue(result);
    }

    @Test
    public void testFolderContainsFolderNegative() {
        root.add(folder);
        boolean result = Searcher.isContains(root, ANOTHER_FOLDER);
        Assert.assertFalse(result);
    }

    @Test
    public void testFolderContainsFile() {
        root.add(file);
        boolean result = Searcher.isContains(root, FILE_NAME);
        Assert.assertTrue(result);
    }

    @Test
    public void testFolderContainsFileNegative() {
        root.add(file);
        boolean result = Searcher.isContains(root, ANOTHER_FILE);
        Assert.assertFalse(result);
    }

    @Test
    public void testSearchFolderInFolder() {
        root.add(folder);
        Element result = Searcher.searchElement(root, FOLDER_NAME);
        Assert.assertEquals(result, folder);
    }

    @Test
    public void testSearchFolderInFolderNegative() {
        root.add(folder);
        Element result = Searcher.searchElement(root, ANOTHER_FOLDER);
        Assert.assertEquals(result, root);
    }

    @Test
    public void testSearchFileInFolder() {
        root.add(file);
        Element result = Searcher.searchElement(root, FILE_NAME);
        Assert.assertEquals(result, file);
    }

    @Test
    public void testSearchFileInFolderNegative() {
        root.add(file);
        Element result = Searcher.searchElement(root, ANOTHER_FILE);
        Assert.assertEquals(result, root);
    }
}